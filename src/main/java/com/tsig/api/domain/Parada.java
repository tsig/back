package com.tsig.api.domain;
import org.locationtech.jts.geom.Geometry;

import java.time.Instant;
import java.util.List;
import javax.persistence.*;

@Entity
public class Parada {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String nombre;
    Boolean estado;
    @ManyToMany
    List<Linea> lineas;
    @Column(columnDefinition = "geometry")
    Geometry punto;
    Instant fechaCreacion;
    Instant fechaModificacion;

    public Parada(String nombre, Boolean estado, List<Linea> lineas, Geometry punto,
                  Instant fechaCreacion, Instant fechaModificacion) {
        this.nombre = nombre;
        this.estado = estado;
        this.lineas = lineas;
        this.punto = punto;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
    }

    public Parada(String nombre, Boolean estado) {
        this.nombre = nombre;
        this.estado = estado;
    }

    public Parada() {

    }

    public String getNombre() {
        return nombre;
    }

    public Boolean getEstado() {
        return estado;
    }

    public List<Linea> getLineas() {
        return lineas;
    }

    public Geometry getPunto() {
        return punto;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public void setLineas(List<Linea> lineas) {
        this.lineas = lineas;
    }

    public void setPunto(Geometry punto) {
        this.punto = punto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Instant fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Instant getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Instant fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
}
