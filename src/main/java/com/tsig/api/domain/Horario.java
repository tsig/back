package com.tsig.api.domain;

import javax.persistence.*;

@Entity
public class Horario {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    @ManyToOne
    @JoinColumn(name="lineas_id")
    Linea idLinea;
    @ManyToOne
    @JoinColumn(name="parada_id")
    Parada idParada;
    String horario;
    boolean habilitado;

    public Horario(Long id, Linea idLinea, Parada idParada, String horario, boolean habilitado) {
        this.id = id;
        this.idLinea = idLinea;
        this.idParada = idParada;
        this.horario = horario;
        this.habilitado = habilitado;
    }

    public Horario(Linea idLinea, Parada idParada, String horario, boolean habilitado) {
        this.idLinea = idLinea;
        this.idParada = idParada;
        this.horario = horario;
        this.habilitado = habilitado;
    }

    public Horario() {
    }


    public Long getId() {
        return id;
    }

    public Linea getIdLinea() {
        return idLinea;
    }

    public Parada getIdParada() {
        return idParada;
    }

    public String getHorario() {
        return horario;
    }

    public boolean isHabilitado() {
        return habilitado;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setIdLinea(Linea idLinea) {
        this.idLinea = idLinea;
    }

    public void setIdParada(Parada idParada) {
        this.idParada = idParada;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }
}

