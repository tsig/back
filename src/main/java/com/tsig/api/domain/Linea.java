package com.tsig.api.domain;

import org.hibernate.annotations.Type;
import org.locationtech.jts.geom.Geometry;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class Linea {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String codigo;
    String origen;
    String destino;
    String empresa;
    Instant fechaCreacion;
    Instant fechaModificacion;
    @Column(columnDefinition = "geometry")
    Geometry recorrido;


    public Linea() {

    }

    public Linea(Long id, String codigo, String origen, String destino, String empresa, Instant fechamodificacion, Instant fechainicio, Geometry recorrido) {
        this.id = id;
        this.codigo = codigo;
        this.origen = origen;
        this.destino = destino;
        this.empresa = empresa;
        this.fechaCreacion = fechainicio;
        this.fechaModificacion = fechamodificacion;
        this.recorrido = recorrido;
    }



    public void setId(Long id) {
        this.id = id;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public void setRecorrido(Geometry recorrido) {
        this.recorrido = recorrido;
    }

    public Long getId() {
        return id;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getOrigen() {
        return origen;
    }

    public String getDestino() {
        return destino;
    }

    public String getEmpresa() {
        return empresa;
    }

    public Geometry getRecorrido() {
        return recorrido;
    }

    public Instant getFechainicio() {
        return fechaCreacion;
    }

    public void setFechainicio(Instant fechainicio) {
        this.fechaCreacion = fechainicio;
    }

    public Instant getFechamodificacion() {
        return fechaModificacion;
    }

    public void setFechamodificacion(Instant fechamodificacion) {
        this.fechaModificacion = fechamodificacion;
    }
}
