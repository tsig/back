package com.tsig.api.repository;

import com.tsig.api.domain.Linea;
import com.tsig.api.domain.Parada;
import org.locationtech.jts.geom.Geometry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioParada extends JpaRepository<Parada, Long> {

    @Query("SELECT p FROM Parada p WHERE :linea MEMBER OF p.lineas")
    Iterable<Parada> findAllByLinea(Linea linea);

    @Query(value = "SELECT DISTINCT st_astext(st_transform(st_intersection(e1.geom, e2.geom), 4326)) FROM ejes e1, ejes e2 WHERE e1.nom_calle=:calleuno AND e2.nom_calle=:calledos", nativeQuery=true)
    List<String> getCoordenadas(String calleuno, String calledos);

    @Query(value = "SELECT id as VARCHAR FROM Parada p WHERE fecha_modificacion >= current_timestamp - (interval '1' hour) * :horas",nativeQuery = true)
    List<String> findUltimasParadasModificadasBy(Long horas);
}