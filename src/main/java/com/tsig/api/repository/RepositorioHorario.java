package com.tsig.api.repository;

import com.tsig.api.controller.dto.HorarioDTO;
import com.tsig.api.domain.Horario;
import com.tsig.api.domain.Parada;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioHorario extends JpaRepository<Horario, Long> {
    @Query(value = "SELECT * FROM Horario h WHERE parada_id = :idParada AND lineas_id = :idLinea", nativeQuery = true)
    List<Horario> findAllByIdParadaAndIdLinea(Long idParada, Long idLinea);

    @Query(value = "SELECT id FROM Horario h WHERE parada_id = :idParada", nativeQuery = true)
    List<Long> findAllHorarioByIdParada(Long idParada);
}
