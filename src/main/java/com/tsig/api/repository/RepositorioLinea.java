package com.tsig.api.repository;

import com.tsig.api.domain.Linea;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioLinea extends JpaRepository<Linea, Long> {

    @Query(value = "SELECT codigo FROM Linea l WHERE fecha_modificacion >= current_timestamp - (interval '1' hour) * :horas",nativeQuery = true)
    List<String> findUltimasLineasModificadasBy(Long horas);

    @Query("SELECT l.codigo, l.origen, l.destino, l.empresa FROM Linea l WHERE l.empresa = :empresa")
    List<String> findLineasPorEmpresa(String empresa);

    @Query(value = "SELECT DISTINCT st_astext(st_intersection(e1.geom, e2.geom)) FROM ejes e1, ejes e2 WHERE e1.nom_calle=:calleuno AND e2.nom_calle=:calledos", nativeQuery=true)
    List<String> getCoordenadas2(String calleuno, String calledos);
}