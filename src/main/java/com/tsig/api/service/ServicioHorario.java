package com.tsig.api.service;
import com.tsig.api.controller.dto.HorarioDTO;
import com.tsig.api.domain.Horario;
import com.tsig.api.domain.Linea;
import com.tsig.api.domain.Parada;
import com.tsig.api.repository.RepositorioHorario;
import com.tsig.api.repository.RepositorioLinea;
import com.tsig.api.repository.RepositorioParada;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ServicioHorario {

    @Autowired
    private RepositorioHorario repositorio;

    @Autowired
    private RepositorioParada repositorioParada;

    @Autowired
    private RepositorioLinea repositorioLinea;

    public void addHorario(HorarioDTO horario) {

        Optional<Parada> parada = repositorioParada.findById(horario.getIdParada());
        Optional<Linea> linea = repositorioLinea.findById(horario.getIdLinea());

        //Se verifica que la parada y la linea existan.
        if(!parada.isPresent() || !linea.isPresent())
            throw new RuntimeException();

        Horario horarios = new Horario(linea.get(),parada.get(),horario.getHorario(),horario.isHabilitado());
        repositorio.save(horarios);

    }
    public void daleteHorario(Long idHorario) {

        Optional<Horario> horario = repositorio.findById(idHorario);
        //se controla la existencia del horario antes de eliminar.
        if(horario.isPresent()) {
        repositorio.delete(horario.get());
        }
        else{
            throw new RuntimeException();
        }
    }
    public void updateHorario(Long idHorario, String hora) {

        Optional<Horario> horario = repositorio.findById(idHorario);
        //se controla la existencia del horario antes de modificar.
        if(horario.isPresent()) {
            Horario updateHorario = horario.get();
            updateHorario.setHorario(hora.replace("\"",""));
            repositorio.save(updateHorario);
        }
        else{
            throw new RuntimeException();
        }
    }

    public List<HorarioDTO> getHorario(Long idParada, Long idLinea) {
        List<Horario> horarios = repositorio.findAllByIdParadaAndIdLinea(idParada,idLinea);
        return horarios.stream().map(horario -> buildHorarioDtoFrom(horario)).collect(Collectors.toList());
    }

    public HorarioDTO buildHorarioDtoFrom(Horario horario) {
        return new HorarioDTO(
                horario.getId(),
                horario.getIdLinea().getId(),
                horario.getIdParada().getId(),
                horario.getHorario(),
                horario.isHabilitado()
        );
    }


}
