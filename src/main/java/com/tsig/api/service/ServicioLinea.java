package com.tsig.api.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsig.api.controller.util.GeometryUtils;
import com.tsig.api.domain.Linea;
import com.tsig.api.repository.RepositorioLinea;
import org.geotools.geojson.geom.GeometryJSON;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import org.opengis.referencing.operation.TransformException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class ServicioLinea {

    private final RepositorioLinea repositorioLinea;

    @Autowired
    private ServicioParada servicioParada;

    @Autowired
    private GeometryUtils geometryUtils;

    @Autowired
    public ServicioLinea(RepositorioLinea repositorioLinea) {
        this.repositorioLinea = repositorioLinea;
    }

    public List<Linea> getLineas(){
        return repositorioLinea.findAll();
    }

    public void agregarLinea(Linea linea) {
        repositorioLinea.save(linea);
    }

    public boolean existeLinea(Long id) {
        return repositorioLinea.existsById(id);
    }

    public void eliminarLinea(Long id) {
        Optional<Linea> lineaOptional = repositorioLinea.findById(id);
        servicioParada.eliminarLinea(lineaOptional.get());
        repositorioLinea.deleteById(id);
    }

    @Transactional
    public void editarRecorrido(Long idLinea, Geometry nuevoRec) {
        Optional<Linea> lineaOptional = repositorioLinea.findById(idLinea);
        Linea linea = lineaOptional.get();
        linea.setRecorrido(nuevoRec);
        Instant nuevaHora = Instant.now();
        linea.setFechamodificacion(nuevaHora);
        servicioParada.eliminarRelacionLineaParada(linea);
    }

    public Geometry jsonToGeom(JsonNode jsonRec) throws IOException, TransformException {
        String json = new ObjectMapper().writeValueAsString(jsonRec);
        GeometryJSON geoJson = new GeometryJSON();
        Reader reader = new StringReader(json);
        LineString recorrido = geoJson.readLine(reader);
        Geometry geomRec = geometryUtils.transformSRID(recorrido);

        return geomRec;
    }

    public List<String> findUltimasLineasModificadas(Long horas) {
        return repositorioLinea.findUltimasLineasModificadasBy(horas);
    }

    public List<String> findLineasPorEmpresa(String empresa) {
        return repositorioLinea.findLineasPorEmpresa(empresa);
    }

    public List<Long> findLineasPorDestino(Geometry puntoUsr, Geometry puntoDir) {
        List<Linea> lineasFiltradas = new ArrayList<>();
        List<Long> finalLineas = new ArrayList<>();
        double buff = 100;

        while(lineasFiltradas.isEmpty() && buff<=1000){
            double finalBuff = buff;
            lineasFiltradas = repositorioLinea.findAll().stream()
                    .filter(linea -> linea.getRecorrido().buffer(finalBuff).intersects(puntoUsr)
                            || linea.getRecorrido().buffer(finalBuff).crosses(puntoUsr)
                            || linea.getRecorrido().buffer(finalBuff).touches(puntoUsr) )
                    .collect(Collectors.toList());
            buff+=100;
        }
        buff = 100;
        if(!lineasFiltradas.isEmpty()) {
            while (finalLineas.isEmpty() && buff <= 1000) {
                //double finalBuff = buff;
                for (Linea linea : lineasFiltradas) {
                    if (linea.getRecorrido().buffer(buff).intersects(puntoDir)
                            || linea.getRecorrido().buffer(buff).crosses(puntoDir)
                            || linea.getRecorrido().buffer(buff).touches(puntoDir)) {

                        finalLineas.add(linea.getId());
                    }
                }
                buff += 100;
            }
        }else{
            while (finalLineas.isEmpty() && buff <= 1000) {
                for (Linea linea : repositorioLinea.findAll()) {
                    if (linea.getRecorrido().buffer(buff).intersects(puntoDir)
                            || linea.getRecorrido().buffer(buff).crosses(puntoDir)
                            || linea.getRecorrido().buffer(buff).touches(puntoDir)) {

                        finalLineas.add(linea.getId());
                    }
                }
                buff += 100;
            }
        }
        return finalLineas;
    }

    public List<Long> findLineasPorPunto(Geometry puntoUsr, double buffer) {
        List<Long> finalLineas = new ArrayList<>();

        for (Linea linea : repositorioLinea.findAll()) {
            if (linea.getRecorrido().buffer(buffer).intersects(puntoUsr)
                    || linea.getRecorrido().buffer(buffer).crosses(puntoUsr)
                    || linea.getRecorrido().buffer(buffer).touches(puntoUsr)) {

                finalLineas.add(linea.getId());
            }
        }
        return finalLineas;
    }

    public List<String> getCoords(String calleuno, String calledos) {
        return repositorioLinea.getCoordenadas2(calleuno,calledos);
    }

    public List<Long> findLineasPorPoligono(Geometry polygon) {
        List<Long> finalLineas = new ArrayList<>();

        for (Linea linea : repositorioLinea.findAll()) {
            if (linea.getRecorrido().intersects(polygon)
                || linea.getRecorrido().crosses(polygon)
                || linea.getRecorrido().touches(polygon)) {

                finalLineas.add(linea.getId());
            }
        }
        return finalLineas;
    }
}