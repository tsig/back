package com.tsig.api.service;
import com.tsig.api.controller.dto.LineaAsocDTO;
import com.tsig.api.controller.dto.ParadaDTO;
import com.tsig.api.controller.dto.ParadaEditDTO;
import com.tsig.api.controller.util.GeometryUtils;
import com.tsig.api.domain.Linea;
import com.tsig.api.domain.Parada;
import com.tsig.api.repository.RepositorioHorario;
import com.tsig.api.repository.RepositorioLinea;
import com.tsig.api.repository.RepositorioParada;
import org.locationtech.jts.geom.Geometry;
import org.opengis.referencing.operation.TransformException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ServicioParada {

    @Autowired
    private RepositorioParada repositorio;

    @Autowired
    private RepositorioLinea repositorioLinea;

    @Autowired
    private RepositorioHorario repositorioHorario;

    @Autowired
    private GeometryUtils geometryUtils;

    public void addParada(ParadaDTO paradaDto) throws IOException, TransformException {

        Geometry pointFromJson = geometryUtils.jsonToPoint(paradaDto.getPunto());
        Geometry pointTransformed = geometryUtils.transformSRID(pointFromJson);


        List<Linea> lineasFiltradas = repositorioLinea.findAll().stream()
                .filter(linea -> linea.getRecorrido().buffer(20).intersects(pointTransformed)
                              || linea.getRecorrido().buffer(20).crosses(pointTransformed)
                              || linea.getRecorrido().buffer(20).touches(pointTransformed) )
                .collect(Collectors.toList());

        boolean estado = true;
        List<Linea> lineas = lineasFiltradas;
        if(lineasFiltradas.size() == 0) {
            estado = false;
            lineas = null;
        }

        Parada parada = new Parada(
                paradaDto.getNombre(),
                estado,
                lineas,
                pointTransformed,
                Instant.now(),
                null
        );

        repositorio.save(parada);

    }
    public void deleteParada(Long id) {
        Optional<Parada> parada = repositorio.findById(id);

        if(parada.isPresent()) {
            List<Long> idHorarios = repositorioHorario.findAllHorarioByIdParada(parada.get().getId());
            idHorarios.forEach(idHorario -> repositorioHorario.deleteById(idHorario));
            repositorio.deleteById(id);
        }
    }

    @Transactional
    public void addLineaToParada(Long id,Long lineaId) {

        Optional<Linea> lineas = repositorioLinea.findById(lineaId);
        Optional<Parada> paradas = repositorio.findById(id);

        if(paradas.isPresent() && lineas.isPresent()){
            paradas.get().getLineas().add(lineas.get());
            paradas.get().setEstado(true);
        }
        else{
            throw new RuntimeException();
        }
    }

    @Transactional
    public void deleteLineaFromParada(Long id,Long lineaId) {

        Optional<Parada> parada = repositorio.findById(id);
        Optional<Linea> linea = repositorioLinea.findById(lineaId);

        if(parada.isPresent()){
            Parada p = parada.get();;
            p.getLineas().removeIf(line -> line.getId().equals(lineaId));
            if (p.getLineas().isEmpty()){
                p.setEstado(false);
            }
        }
        else{
            throw new RuntimeException();
        }

    }

    public boolean existsParada(Long id) {
        return repositorio.existsById(id);
    }

    @Transactional
    public void eliminarLinea(Linea linea){
        Iterable<Parada> paradasbylinea = repositorio.findAllByLinea(linea);
        paradasbylinea.forEach((p) ->{
            p.getLineas().remove(linea);
            if (p.getLineas().isEmpty()){
                p.setEstado(false);
            }
        });
    }
    public void editParada(ParadaEditDTO paradaDto, Long id ) throws IOException, TransformException {

        Geometry pointFromJson = geometryUtils.jsonToPoint(paradaDto.getPunto());
        Geometry pointTransformed = geometryUtils.transformSRID(pointFromJson);

       Optional<Parada> parada = repositorio.findById(id);

       List<Linea> lineasFiltradas = repositorioLinea.findAll().stream()
                .filter(linea -> linea.getRecorrido().buffer(20).intersects(pointTransformed)
                        || linea.getRecorrido().buffer(20).crosses(pointTransformed)
                        || linea.getRecorrido().buffer(20).touches(pointTransformed) )
                .collect(Collectors.toList());

       if(parada.isPresent()) {
           Parada updateParada = parada.get();
           boolean estado = true;
           List<Linea> lineas = lineasFiltradas;
           if(lineasFiltradas.size() == 0) {
               lineas = null;
               estado = false;
           }
            updateParada.setLineas(lineas);
            updateParada.setPunto(pointTransformed);
            updateParada.setEstado(estado);
            updateParada.setFechaModificacion(Instant.now());
            repositorio.save(updateParada);
       }
       else{
           throw new RuntimeException();
       }

    }
    @Transactional
    public void eliminarRelacionLineaParada(Linea linea){
        Iterable<Parada> paradasbylinea = repositorio.findAllByLinea(linea);
        System.out.println(linea.getRecorrido().toText());
        paradasbylinea.forEach((p) ->{
            System.out.println(p.getNombre());
            if(p.getPunto().buffer(10).intersects(linea.getRecorrido())
            || p.getPunto().buffer(10).crosses(linea.getRecorrido())
            || p.getPunto().buffer(10).touches(linea.getRecorrido()))
            { //funciona al reves
                p.getLineas().remove(linea);
            }
            if (p.getLineas().isEmpty()){
                p.setEstado(false);
            }
        });
    }

    public List<LineaAsocDTO> findAllByIdParada(Long idParada) {
        Optional<Parada> optionalParada = repositorio.findById(idParada);
        if(optionalParada.isPresent()) {
            Parada parada = optionalParada.get();
            List<Linea> lineasParada = parada.getLineas();
            List<Linea> lineas = repositorioLinea.findAll();
            Geometry puntoParada = parada.getPunto();

            List<Linea> lineasFiltradas = repositorioLinea.findAll().stream()
                    .filter(linea -> linea.getRecorrido().buffer(20).intersects(puntoParada)
                            || linea.getRecorrido().buffer(20).crosses(puntoParada)
                            || linea.getRecorrido().buffer(20).touches(puntoParada) )
                    .collect(Collectors.toList());

            return lineasFiltradas.stream().map(linea -> {
                boolean estaAsociada = lineasParada.stream().anyMatch(lineaParada -> lineaParada.getCodigo().equals(linea.getCodigo()));
                return new LineaAsocDTO(
                        linea.getId(),
                        linea.getCodigo(),
                        linea.getOrigen(),
                        linea.getDestino(),
                        linea.getEmpresa(),
                        estaAsociada
                );
            }).collect(Collectors.toList());

        } else {
            return Collections.emptyList();
        }
    }

    public List<String> findUltimasParadasModificadas(Long horas) {
        return repositorio.findUltimasParadasModificadasBy(horas);
    }

    public List<String> getCoordenadas(String calleuno, String calledos) {
        return repositorio.getCoordenadas(calleuno, calledos);
    }
}
