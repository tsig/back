package com.tsig.api.service;

import com.tsig.api.domain.Usuario;
import com.tsig.api.repository.RepositorioUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ServicioUsuario {

    @Autowired
    private RepositorioUsuario repositorio;

    public void crearUsuario(Usuario usuario) {
        repositorio.save(usuario);
    }

    public boolean login(String username, String password) {
        Optional<Usuario> user = repositorio.findByUsername(username);
        return user.map(value -> value.getPassword().equals(password)).orElse(false);
    }
}
