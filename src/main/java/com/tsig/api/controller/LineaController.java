package com.tsig.api.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.tsig.api.controller.dto.LineaDTO;
import com.tsig.api.controller.util.GeometryUtils;
import com.tsig.api.domain.Linea;
import com.tsig.api.service.ServicioLinea;
import org.geotools.geometry.jts.JTS;
import org.locationtech.jts.geom.Geometry;
import org.opengis.referencing.operation.TransformException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin(origins="*", allowedHeaders = "*")
@RequestMapping(path = "lineas")
public class LineaController {

    private final ServicioLinea servicioLinea;

    @Autowired
    GeometryUtils geometryUtils;

    @Autowired
    public LineaController(ServicioLinea servicioLinea) {
        this.servicioLinea = servicioLinea;
    }

    @GetMapping("/getlineas")
    public List<Linea> getLineas(){
        return servicioLinea.getLineas();
    }

    @PostMapping("/crearlinea")
    public ResponseEntity<String> crearLinea (@RequestBody LineaDTO linea) throws IOException, TransformException {
        Geometry geomRecorrido = servicioLinea.jsonToGeom(linea.getRecorrido());

        Linea lineToSave = new Linea(
                new Long(0),
                linea.getCodigo(),
                linea.getOrigen(),
                linea.getDestino(),
                linea.getEmpresa(),
                linea.getFechainicio(),
                linea.getFechamodificacion(),
                geomRecorrido

        );
        servicioLinea.agregarLinea(lineToSave);
        return new ResponseEntity<>("linea creada OK", HttpStatus.OK);
    }

    @DeleteMapping(path = "{lineaId}")
    public ResponseEntity<String> eliminarLinea(@PathVariable("lineaId") Long id){
        if(!servicioLinea.existeLinea(id)){
            return new ResponseEntity<>("no existe la linea", HttpStatus.BAD_REQUEST);
        }
        servicioLinea.eliminarLinea(id);
        return new ResponseEntity<>("linea eliminada OK", HttpStatus.OK);
    }

    @PutMapping("/editar/{lineaId}")
    public ResponseEntity<String> editarRecorrido(@PathVariable("lineaId") Long idLinea, @RequestBody JsonNode jsonRec) throws IOException, TransformException {
        Geometry nuevoRec = servicioLinea.jsonToGeom(jsonRec);

        if(!servicioLinea.existeLinea(idLinea)){
            return new ResponseEntity<>("no existe la linea", HttpStatus.BAD_REQUEST);
        }
        servicioLinea.editarRecorrido(idLinea, nuevoRec);
        return new ResponseEntity<>("recorrido de linea actualizado OK", HttpStatus.OK);
    }

    @GetMapping("/ultimas/{horas}")
    public ResponseEntity<List<String>> ultimasLineasModificadas(@PathVariable("horas") Long horas) {
        return new ResponseEntity<>(servicioLinea.findUltimasLineasModificadas(horas), HttpStatus.OK);
    }

    @GetMapping("/xEmpresa/{empresa}")
    public ResponseEntity<List<String>> lineasPorEmpresa(@PathVariable("empresa") String empresa) {
        return new ResponseEntity<>(servicioLinea.findLineasPorEmpresa(empresa), HttpStatus.OK);
    }

    @GetMapping("xDestino/{puntoUsuario}/{puntoDireccion}")
    public ResponseEntity<List<Long>> lineasPorDestino(@RequestBody JsonNode usrJSON, @RequestBody JsonNode dirJSON) throws IOException {
        Geometry puntoUsr = geometryUtils.jsonToPoint(usrJSON);
        Geometry puntoDir = geometryUtils.jsonToPoint(dirJSON);

        return new ResponseEntity<>(servicioLinea.findLineasPorDestino(puntoUsr, puntoDir), HttpStatus.OK);
    }

    @GetMapping("/getejes/{calleuno}/{calledos}")
    public List<String> getejes2(@PathVariable("calleuno")String calleuno, @PathVariable("calledos")String calledos){
        return servicioLinea.getCoords(calleuno, calledos);
    }

    @PutMapping("posUsuario/{buffer}")
    public ResponseEntity<List<Long>> lineasPorUsuario(@RequestBody JsonNode usrJSON, @PathVariable("buffer") double buffer) throws IOException, TransformException {
        Geometry puntoUsr = geometryUtils.jsonToPoint(usrJSON);

        return new ResponseEntity<>(servicioLinea.findLineasPorPunto(puntoUsr, buffer), HttpStatus.OK);
    }

    @PostMapping("poligono")
    public ResponseEntity<List<Long>> lineasPoligono(@RequestBody JsonNode poligono) throws TransformException, IOException {
        Geometry polygon = geometryUtils.jsonToPolygon(poligono);

        return new ResponseEntity<>(servicioLinea.findLineasPorPoligono(polygon), HttpStatus.OK);
    }
}
