package com.tsig.api.controller;

import com.google.gson.Gson;
import com.tsig.api.controller.response.LoginResponse;
import com.tsig.api.domain.Usuario;
import com.tsig.api.service.ServicioUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@CrossOrigin(origins="*", allowedHeaders = "*")
@RequestMapping(path = "user")
public class UsuarioController {

    @Autowired
    private ServicioUsuario servicioUsuario;


    @PostMapping("/add")
    public ResponseEntity<String> crearUsuario(@RequestBody Usuario usuario) {
        servicioUsuario.crearUsuario(usuario);
        return new ResponseEntity<>("usuario creado OK", HttpStatus.OK);
    }

    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> login(@RequestBody Usuario usuario) throws IOException {
        if(usuario.getUsername() == null || usuario.getPassword() == null) {
            return new ResponseEntity<>("Usuario y/o password invalido", HttpStatus.BAD_REQUEST);
        }

        boolean result = servicioUsuario.login(usuario.getUsername(), usuario.getPassword());
        if(result) {
            LoginResponse okResponse = new LoginResponse(usuario.getUsername(), true);
            return new ResponseEntity<>(new Gson().toJson(okResponse), HttpStatus.OK);
        }else {
            LoginResponse errResponse = new LoginResponse(usuario.getUsername(), false);
            return new ResponseEntity<>(new Gson().toJson(errResponse), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }





}
