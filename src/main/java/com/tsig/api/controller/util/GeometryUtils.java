package com.tsig.api.controller.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.geotools.geojson.geom.GeometryJSON;
import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Polygon;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

@Component
public class GeometryUtils {

    private CoordinateReferenceSystem sourceCRS = CRS.decode("EPSG:3857");
    private CoordinateReferenceSystem targetCRS = CRS.decode("EPSG:32721");
    private MathTransform transformation = CRS.findMathTransform(sourceCRS, targetCRS);

    public GeometryUtils() throws FactoryException {
    }

    public Geometry transformSRID(Geometry target) throws TransformException {
        return JTS.transform(target, transformation);
    }

    public Geometry jsonToPoint(JsonNode geoJson) throws IOException {
        String geoJsonAsString = new ObjectMapper().writeValueAsString(geoJson);
        GeometryJSON geometryJson = new GeometryJSON();
        Reader reader = new StringReader(geoJsonAsString);
        return geometryJson.readPoint(reader);
    }

    public Geometry jsonToPolygon(JsonNode jsonRec) throws IOException, TransformException {
        String json = new ObjectMapper().writeValueAsString(jsonRec);
        GeometryJSON geoJson = new GeometryJSON();
        Reader reader = new StringReader(json);
        Polygon poligono = geoJson.readPolygon(reader);
        Geometry geom = this.transformSRID(poligono);

        return geom;
    }

}
