package com.tsig.api.controller.response;

public class LoginResponse {

    private String username;
    private boolean isLogged;

    public LoginResponse(String username, boolean isLogged) {
        this.username = username;
        this.isLogged = isLogged;
    }
}