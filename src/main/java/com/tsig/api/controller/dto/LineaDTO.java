package com.tsig.api.controller.dto;


import com.fasterxml.jackson.databind.JsonNode;

import java.time.Instant;

public class LineaDTO {

    Long id;
    String codigo;
    String origen;
    String destino;
    String empresa;
    Instant fechainicio;
    Instant fechamodificacion;
    JsonNode recorrido;


    public Long getId() {
        return id;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getOrigen() {
        return origen;
    }

    public String getDestino() {
        return destino;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public JsonNode getRecorrido() {
        return recorrido;
    }

    public void setRecorrido(JsonNode recorrido) {
        this.recorrido = recorrido;
    }

    public Instant getFechainicio() {
        return fechainicio;
    }

    public Instant getFechamodificacion() {
        return fechamodificacion;
    }

    public void setFechainicio(Instant fechainicio) {
        this.fechainicio = fechainicio;
    }

    public void setFechamodificacion(Instant fechamodificacion) {
        this.fechamodificacion = fechamodificacion;
    }
}
