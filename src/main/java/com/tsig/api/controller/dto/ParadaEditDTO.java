package com.tsig.api.controller.dto;

import com.fasterxml.jackson.databind.JsonNode;

public class ParadaEditDTO {
    JsonNode punto;

    public JsonNode getPunto() {
        return punto;
    }

    public void setPunto(JsonNode punto) {
        this.punto = punto;
    }
}
