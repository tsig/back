package com.tsig.api.controller.dto;

import com.fasterxml.jackson.databind.JsonNode;

public class ParadaDTO {
    Long id;
    String nombre;
    JsonNode punto;

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public JsonNode getPunto() {
        return punto;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
