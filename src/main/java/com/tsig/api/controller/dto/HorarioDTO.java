package com.tsig.api.controller.dto;

public class HorarioDTO {

    Long idHorario;
    Long idLinea;
    Long idParada;
    String horario;
    boolean habilitado;

    public HorarioDTO() { }

    public HorarioDTO(Long idHorario, Long idLinea, Long idParada, String horario, boolean habilitado) {
        this.idHorario = idHorario;
        this.idLinea = idLinea;
        this.idParada = idParada;
        this.horario = horario;
        this.habilitado = habilitado;
    }

    public HorarioDTO(Long idLinea, Long idParada, String horario, boolean habilitado) {
        this.idLinea = idLinea;
        this.idParada = idParada;
        this.horario = horario;
        this.habilitado = habilitado;
    }

    public Long getIdLinea() {
        return idLinea;
    }

    public Long getIdParada() {
        return idParada;
    }

    public String getHorario() {
        return horario;
    }

    public boolean isHabilitado() {
        return habilitado;
    }

    public void setIdLinea(Long idLinea) {
        this.idLinea = idLinea;
    }

    public void setIdParada(Long idParada) {
        this.idParada = idParada;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }

    public Long getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(Long idHorario) {
        this.idHorario = idHorario;
    }
}
