package com.tsig.api.controller.dto;

public class LineaAsocDTO {

    Long id;
    String codigo;
    String origen;
    String destino;
    String empresa;
    boolean asociada;

    public LineaAsocDTO(Long id, String codigo, String origen, String destino, String empresa, boolean asociada) {
        this.id = id;
        this.codigo = codigo;
        this.origen = origen;
        this.destino = destino;
        this.empresa = empresa;
        this.asociada = asociada;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public boolean isAsociada() {
        return asociada;
    }

    public void setAsociada(boolean asociada) {
        this.asociada = asociada;
    }
}
