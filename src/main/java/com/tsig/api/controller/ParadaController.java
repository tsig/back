package com.tsig.api.controller;


import com.tsig.api.controller.dto.LineaAsocDTO;
import com.tsig.api.controller.dto.ParadaDTO;
import com.tsig.api.controller.dto.ParadaEditDTO;
import com.tsig.api.service.ServicioParada;
import org.opengis.referencing.operation.TransformException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin(origins="*", allowedHeaders = "*")
@RequestMapping(path = "paradas")
public class ParadaController {

    @Autowired
    private ServicioParada servicioParada;

    @PostMapping("/add")
    public ResponseEntity<String> addParada (@RequestBody ParadaDTO parada) throws IOException, TransformException {
        servicioParada.addParada(parada);
        return new ResponseEntity<>("Parada creada OK", HttpStatus.OK);
    }

    @DeleteMapping(path = "/{paradaId}")
    public ResponseEntity<String> delateParada(@PathVariable("paradaId") Long id){
        if(!servicioParada.existsParada(id)){
            return new ResponseEntity<>("no existe la Parada", HttpStatus.BAD_REQUEST);
        }
        servicioParada.deleteParada(id);
        return new ResponseEntity<>("Parada eliminada correctamente", HttpStatus.OK);
    }
    @PostMapping("/{paradaId}/addLineaToParada/{lineaId}")
    public ResponseEntity<String> addLineaToParada (@PathVariable("paradaId") Long id, @PathVariable("lineaId") Long lineaId){

        servicioParada.addLineaToParada(id,lineaId);
        return new ResponseEntity<>("Linea agregada a la parada OK", HttpStatus.OK);
    }

    @DeleteMapping("/{paradaId}/linea/{lineaId}")
    public ResponseEntity<String> deleteLineaFromParada (@PathVariable("paradaId") Long id, @PathVariable("lineaId") Long lineaId){

        servicioParada.deleteLineaFromParada(id,lineaId);
        return new ResponseEntity<>("Linea eliminada de la parada OK", HttpStatus.OK);
    }

    @PutMapping("/editParada/{paradaId}")
    public ResponseEntity<String> editParada(@RequestBody ParadaEditDTO parada, @PathVariable("paradaId") Long id) throws IOException, TransformException {
        servicioParada.editParada(parada,id);
        return new ResponseEntity<>("Parada modificada OK", HttpStatus.OK);
    }

    @GetMapping("/{paradaId}/lineas")
    public ResponseEntity<List<LineaAsocDTO>> lineasPorParada(@PathVariable("paradaId") Long paradaId) {
        return new ResponseEntity<>(servicioParada.findAllByIdParada(paradaId), HttpStatus.OK);
    }

    @GetMapping("/ultimas/{horas}")
    public ResponseEntity<List<String>> ultimasParadasModificadas(@PathVariable("horas") Long horas) {
        return new ResponseEntity<>(servicioParada.findUltimasParadasModificadas(horas), HttpStatus.OK);
    }

    @GetMapping("/getejes/{calleuno}/{calledos}")
    public List<String> getejes(@PathVariable("calleuno")String calleuno, @PathVariable("calledos")String calledos){
        return servicioParada.getCoordenadas(calleuno, calledos);
    }
 }
