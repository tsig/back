package com.tsig.api.controller;

import com.tsig.api.controller.dto.HorarioDTO;
import com.tsig.api.domain.Horario;
import com.tsig.api.service.ServicioHorario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins="*", allowedHeaders = "*")
@RequestMapping(path = "horario")
public class HorarioController {

    @Autowired
    private ServicioHorario servicioHorario;
    @PostMapping("/add")
    public ResponseEntity<String> addHorario(@RequestBody HorarioDTO horario) {

        servicioHorario.addHorario(horario);
        return new ResponseEntity<>("Horario creado OK", HttpStatus.OK);
    }
    @DeleteMapping("/delete/{idHorario}")
    public ResponseEntity<String> deleteHorario(@PathVariable("idHorario") Long idHorario) {

        servicioHorario.daleteHorario(idHorario);
        return new ResponseEntity<>("Horario fue borrado correctamente", HttpStatus.OK);
    }
    @PutMapping("/update/{idHorario}")
    public ResponseEntity<String> updateHorario(@PathVariable ("idHorario") Long idHorario, @RequestBody String hora) {

        servicioHorario.updateHorario(idHorario, hora);
        return new ResponseEntity<>("Horario fue modificado correctamente", HttpStatus.OK);
    }
    @GetMapping("/getHorario/{idParada}/{idLinea}")
    public ResponseEntity<List<HorarioDTO>> getHorario(@PathVariable ("idParada") Long idParada, @PathVariable("idLinea") Long idLinea) {
        servicioHorario.getHorario(idParada, idLinea);
        return new ResponseEntity<>(servicioHorario.getHorario(idParada, idLinea), HttpStatus.OK);
    }
}


