INSERT INTO usuario (id, username, password) VALUES ('1', 'admin', 'admin');

INSERT INTO linea (id, codigo, destino, empresa, origen, fecha_creacion, fecha_modificacion, recorrido) VALUES (1, '526', 'Buceo', 'COME', 'Manga', (select CURRENT_TIMESTAMP), (select CURRENT_TIMESTAMP - interval '1 hour'), (select st_geomfromtext('LINESTRING(576080.6236502774 6140849.2603132725,576137.7469987568 6140138.908021959,575326.5291428172 6140095.193907741,574481.186591716 6140475.398509696)', 32721)));
INSERT INTO linea (id, codigo, destino, empresa, origen, fecha_creacion, fecha_modificacion, recorrido) VALUES (2, '185', 'Santa Catalina', 'CUTSCA', 'Pocitos',(SELECT CURRENT_TIMESTAMP), (select CURRENT_TIMESTAMP - interval '1 days'), (select st_geomfromtext('LINESTRING(575476.1198318982 6141031.584198331,572906.5686824389 6140838.640026435,573099.8967232807 6140516.550431163,573396.6940350943 6140268.117479807,573430.2172114734 6140110.813882074,573563.9826803405 6139691.478137255,573768.0787310624 6139348.971766965,574140.501017833 6138998.0572313005,574295.0408710234 6138821.994567653,574767.9643374 6138867.653893739,574933.0993401569 6139290.453293843)',32721)));
INSERT INTO linea (id, codigo, destino, empresa, origen, fecha_creacion, fecha_modificacion, recorrido) VALUES (3, '187', 'Ciudad Vieja', 'CUTSCA', 'Palacio de la luz',(SELECT CURRENT_TIMESTAMP), (select CURRENT_TIMESTAMP - interval '1 hour'), (select st_geomfromtext('LINESTRING(576198.007775208 6139453.118878057,576269.6040871916 6137958.231793564,574482.4938164342 6137139.088492196,573262.07058847 6137089.279349547,572342.9256121737 6136756.346527992)',32721)));
INSERT INTO linea (id, codigo, destino, empresa, origen, fecha_creacion, fecha_modificacion, recorrido) VALUES (4, '71', 'Portones', 'RAINCOOP', 'Parque Batlle',(SELECT CURRENT_TIMESTAMP), (select CURRENT_TIMESTAMP - interval '1 days'), (select st_geomfromtext('LINESTRING(576323.7349980136 6138324.249325575,578189.8071506465 6139095.929010313,578938.808402868 6139200.004006968,581269.6438040098 6139082.088834455,583128.828925192 6139482.190374109,583832.0331645219 6139514.2113272585,585154.4878639014 6140041.853720263)',32721)));
INSERT INTO linea (id, codigo, destino, empresa, origen, fecha_creacion, fecha_modificacion, recorrido) VALUES (5, '329', 'Pocitos', 'UCOT', 'Parque Batlle',(SELECT CURRENT_TIMESTAMP), null, (select st_geomfromtext('LINESTRING(576383.5311849215 6137542.145300791,577390.2802886764 6137354.980092015,578550.8649767359 6137456.59807039)',32721)));
INSERT INTO linea (id, codigo, destino, empresa, origen, fecha_creacion, fecha_modificacion, recorrido) VALUES (6, '405', 'Maroñas', 'COETC', 'Union',(SELECT CURRENT_TIMESTAMP), null, (select st_geomfromtext('LINESTRING(577810.0805425576 6140026.629047884,578986.2448984788 6140774.218451689,579204.0492248413 6141964.494679211,579225.0632848857 6142453.577154749,579820.7275481038 6143329.343033439,580029.6585339789 6144297.085811223,581251.3113362011 6145034.626472659,581493.781224056 6145682.306252703,582198.6119758468 6146124.470857009,582658.2686120904 6147190.596329171)',32721)));
INSERT INTO linea (id, codigo, destino, empresa, origen, fecha_creacion, fecha_modificacion, recorrido) VALUES (7, '328', 'Sayago', 'UCOT', 'Brazo Oriental',(SELECT CURRENT_TIMESTAMP), null, (select st_geomfromtext('LINESTRING(575548.6972698756 6141576.588716522,574398.0687790172 6142151.693775713,573479.3598668854 6142407.307095312,573256.667269433 6143569.732993645,571204.0804213064 6145010.262936795)',32721)));
INSERT INTO linea (id, codigo, destino, empresa, origen, fecha_creacion, fecha_modificacion, recorrido) VALUES (8, '183', 'Rambla', 'CUTSCA', 'Pocitos',(SELECT CURRENT_TIMESTAMP), null, (select st_geomfromtext('LINESTRING(576431.2768942188 6136722.258966334,576452.5756154018 6136435.548312328,577717.3063408593 6135982.471917159,578137.555896896 6136561.279204015,578629.9506286366 6136557.058838742)',32721)));


INSERT INTO parada (id, estado, nombre, punto, fecha_creacion, fecha_modificacion) VALUES (1, false, 'Parada Uno', (select st_geomfromtext('POINT(576080.6236502774 6140849.2603132725)',32721)), (SELECT CURRENT_TIMESTAMP), (select CURRENT_TIMESTAMP - interval '1 hour'));
INSERT INTO parada (id, estado, nombre, punto, fecha_creacion, fecha_modificacion) VALUES (2, false, 'Parada Dos', (select st_geomfromtext('POINT(573099.8967232807 6140516.550431163)',32721)), (SELECT CURRENT_TIMESTAMP), (select CURRENT_TIMESTAMP - interval '1 days'));
INSERT INTO parada (id, estado, nombre, punto, fecha_creacion, fecha_modificacion) VALUES (3, false, 'Parada Pedernal', (select st_geomfromtext('POINT(576104.585669717 6140533.886857456)',32721)), (SELECT CURRENT_TIMESTAMP), (select CURRENT_TIMESTAMP - interval '1 hour'));
INSERT INTO parada (id, estado, nombre, punto, fecha_creacion, fecha_modificacion) VALUES (4, false, 'Parada Colorado', (select st_geomfromtext('POINT(575667.6170684028 6140109.89025157)',32721)), (SELECT CURRENT_TIMESTAMP), (select CURRENT_TIMESTAMP - interval '1 days'));
INSERT INTO parada (id, estado, nombre, punto, fecha_creacion, fecha_modificacion) VALUES (5, true, 'Parada Arenal Grande', (select st_geomfromtext('POINT(575308.7325784197 6140102.690675702)',32721)), (SELECT CURRENT_TIMESTAMP), (select CURRENT_TIMESTAMP - interval '1 hour'));
INSERT INTO parada (id, estado, nombre, punto, fecha_creacion, fecha_modificacion) VALUES (6, false, 'Parada Mariano Moreno', (select st_geomfromtext('POINT(576426.0070408917 6140221.251701141)',32721)), (SELECT CURRENT_TIMESTAMP), (select CURRENT_TIMESTAMP - interval '1 days'));
INSERT INTO parada (id, estado, nombre, punto, fecha_creacion, fecha_modificacion) VALUES (7, false, 'Parada Parque Central', (select st_geomfromtext('POINT(576788.9877290555 6139511.2228589505)',32721)), (SELECT CURRENT_TIMESTAMP), (select CURRENT_TIMESTAMP - interval '1 hour'));
INSERT INTO parada (id, estado, nombre, punto, fecha_creacion, fecha_modificacion) VALUES (8, true, 'Parada L.A de Herrera', (select st_geomfromtext('POINT(575171.3003858834 6141761.495398022)',32721)), null, null);
INSERT INTO parada (id, estado, nombre, punto, fecha_creacion, fecha_modificacion) VALUES (9, true, 'Parada Millan', (select st_geomfromtext('POINT(573441.1433458051 6142577.942137522)',32721)), null, null);
INSERT INTO parada (id, estado, nombre, punto, fecha_creacion, fecha_modificacion) VALUES (10, true, 'Parada Palacio Legislativo', (select st_geomfromtext('POINT(574188.5029700781 6138927.996103806)',32721)), null, null);
INSERT INTO parada (id, estado, nombre, punto, fecha_creacion, fecha_modificacion) VALUES (11, true, 'Parada Ferrer Serra', (select st_geomfromtext('POINT(576256.4861354812 6138337.615851734)',32721)), null, null);
INSERT INTO parada (id, estado, nombre, punto, fecha_creacion, fecha_modificacion) VALUES (12, false, 'Parada Rivera', (select st_geomfromtext('POINT(575438.843650685 6137551.305672867)',32721)), null, null);
INSERT INTO parada (id, estado, nombre, punto, fecha_creacion, fecha_modificacion) VALUES (13, true, 'Parada Bvar España', (select st_geomfromtext('POINT(576828.9353287577 6136291.216313807)',32721)), null, null);
INSERT INTO parada (id, estado, nombre, punto, fecha_creacion, fecha_modificacion) VALUES (14, true, 'Parada Larravide', (select st_geomfromtext('POINT(578700.9978932391 6140595.111211392)',32721)), null, null);
INSERT INTO parada (id, estado, nombre, punto, fecha_creacion, fecha_modificacion) VALUES (15, false, 'Parada Rambla', (select st_geomfromtext('POINT(577937.1803238987 6136244.7203365555)',32721)), null, null);
INSERT INTO parada (id, estado, nombre, punto, fecha_creacion, fecha_modificacion) VALUES (16, true, 'Parada Propios', (select st_geomfromtext('POINT(578776.9452403623 6139180.320269789)',32721)), null, null);
INSERT INTO parada (id, estado, nombre, punto, fecha_creacion, fecha_modificacion) VALUES (17, true, 'Parada Hipolito Irigoyen', (select st_geomfromtext('POINT(581285.6521905115 6139087.357692993)',32721)), null, null);

INSERT INTO parada_lineas (parada_id, lineas_id) VALUES (1,1);
INSERT INTO parada_lineas (parada_id, lineas_id) VALUES (1,2);
INSERT INTO parada_lineas (parada_id, lineas_id) VALUES (2,1);
INSERT INTO parada_lineas (parada_id, lineas_id) VALUES (2,2);
INSERT INTO parada_lineas (parada_id, lineas_id) VALUES (3,1);
INSERT INTO parada_lineas (parada_id, lineas_id) VALUES (4,1);
INSERT INTO parada_lineas (parada_id, lineas_id) VALUES (5,1);
INSERT INTO parada_lineas (parada_id, lineas_id) VALUES (8,7);
INSERT INTO parada_lineas (parada_id, lineas_id) VALUES (9,7);
INSERT INTO parada_lineas (parada_id, lineas_id) VALUES (10,2);
INSERT INTO parada_lineas (parada_id, lineas_id) VALUES (11,3);
INSERT INTO parada_lineas (parada_id, lineas_id) VALUES (13,8);
INSERT INTO parada_lineas (parada_id, lineas_id) VALUES (14,6);
INSERT INTO parada_lineas (parada_id, lineas_id) VALUES (16,4);
INSERT INTO parada_lineas (parada_id, lineas_id) VALUES (17,4);

INSERT INTO horario (id, habilitado, horario, lineas_id, parada_id) VALUES (0, true, '23:30', 1, 1);
INSERT INTO horario (id, habilitado, horario, lineas_id, parada_id) VALUES (1, true, '23:45', 1, 1);

SELECT setval('hibernate_sequence', 100, true);